﻿using UnityEngine;
using UnityEngine.EventSystems;

using System;
using System.Collections;
using System.Collections.Generic;

using Vanilla;
using Vanilla.Inputs;
using Vanilla.EventSystems;

namespace Vanilla.VR {
	public class VRPointer : WorldAndCanvasPointer {
		[Header("[ VR Pointer ]")]
		public VRMount mount;

		public override void Awake() {
			base.Awake();

			mount = GetComponent<VRMount>();
		}

		public override void BuildPointerEventData() {
			base.BuildPointerEventData();

			switch (mount.tracker.handedness) {
				case Handedness.Left:
					pointerEventData.button = PointerEventData.InputButton.Left;
					break;
				case Handedness.Right:
					pointerEventData.button = PointerEventData.InputButton.Right;
					break;
				case Handedness.Middle:
					pointerEventData.button = PointerEventData.InputButton.Middle;
					break;
			}
		}

		public override void OnEnable() {
			base.OnEnable();

			switch (mount.tracker.handedness) {
				case Handedness.Left:
					VanillaInputs.i.Subscribe(ProjectAction.LeftInteractorClick, DownHandler);
					VanillaInputs.i.Subscribe(ProjectAction.LeftInteractorRelease, UpHandler);
					break;
				case Handedness.Right:
					VanillaInputs.i.Subscribe(ProjectAction.RightInteractorClick, DownHandler);
					VanillaInputs.i.Subscribe(ProjectAction.RightInteractorRelease, UpHandler);
					break;
				case Handedness.Middle:
					if (line) {
						Destroy(line);
					}
					break;
			}
		}

		public override void OnDisable() {
			base.OnDisable();

			switch (mount.tracker.handedness) {
				case Handedness.Left:
					VanillaInputs.i.Unsubscribe(ProjectAction.LeftInteractorClick, DownHandler);
					VanillaInputs.i.Unsubscribe(ProjectAction.LeftInteractorRelease, UpHandler);
					break;
				case Handedness.Right:
					VanillaInputs.i.Unsubscribe(ProjectAction.RightInteractorClick, DownHandler);
					VanillaInputs.i.Unsubscribe(ProjectAction.RightInteractorRelease, UpHandler);
					break;
			}
		}

		public override bool ModuleHitIsNotEnteredByAnotherOfTheSameModule(PointerModuleType moduleType) {
			return base.ModuleHitIsNotEnteredByAnotherOfTheSameModule(moduleType);
		}
	}
}