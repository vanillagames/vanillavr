﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

using System;

namespace Vanilla.VR {
	public class VRPointerEventsFilter : VanillaBehaviour, IPointerEnterHandler, IPointerExitHandler, IPointerDownHandler, IPointerUpHandler {

		public PointerEventData.InputButton whichControllerToListenTo;

		public bool leftAndRightAreInterchangeable;

		public UnityEvent onPointerEnter;
		public UnityEvent onPointerExit;
		public UnityEvent onPointerDown;
		public UnityEvent onPointerUp;

		void Awake() {
			if (onPointerEnter == null) {
				onPointerEnter = new UnityEvent();
			}

			if (onPointerExit == null) {
				onPointerExit = new UnityEvent();
			}

			if (onPointerDown == null) {
				onPointerDown = new UnityEvent();
			}

			if (onPointerUp == null) {
				onPointerUp = new UnityEvent();
			}
		}

		public void OnPointerEnter(PointerEventData eventData) {
			if (!ControllerIsCompatibleWithPointer((int)eventData.button)) {
				return;
			}

			onPointerEnter.Invoke();
		}

		public void OnPointerExit(PointerEventData eventData) {
			if (!ControllerIsCompatibleWithPointer((int)eventData.button)) {
				return;
			}

			onPointerExit.Invoke();
		}

		public void OnPointerDown(PointerEventData eventData) {
			if (!ControllerIsCompatibleWithPointer((int)eventData.button)) {
				return;
			}

			onPointerDown.Invoke();
		}

		public void OnPointerUp(PointerEventData eventData) {
			if (!ControllerIsCompatibleWithPointer((int)eventData.button)) {
				return;
			}

			onPointerUp.Invoke();
		}

		bool ControllerIsCompatibleWithPointer(int eventButtonID) {
			switch ((int)whichControllerToListenTo) {
				case 0:
					return leftAndRightAreInterchangeable ? eventButtonID != 2 : eventButtonID == 0;
				case 1:
					return leftAndRightAreInterchangeable ? eventButtonID != 2 : eventButtonID == 1;
				case 2:
					return eventButtonID == 2;
				default:
					return true;
			}
		}
	}
}