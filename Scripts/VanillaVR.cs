﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.XR;

using System;
using System.Collections;
using System.Collections.Generic;

using Vanilla.CustomEvents;

namespace Vanilla.VR {
	[Serializable]
	public class VanillaVR : VanillaSystemModule<VanillaVR> {

		[Header("Rig Scenes")]
		public VRRigScene rigScene;

		[Header("VR Rig Profiles")]

		public VRModelProfile currentRigProfile;

		public VRModelProfile[] modelProfiles;

		[Header("Status")]
		[ReadOnly]
		public bool rigProfileMatchFound;

		[Header("VR Tracker Scenes")]

		public VanillaSceneAsset leftTrackerScene;
		public VanillaSceneAsset rightTrackerScene;
		public VanillaSceneAsset hmdTrackerScene;

		[Header("VR Tracker Objects")]

		[ReadOnly]
		public VRTracker leftTracker;
		[ReadOnly]
		public VRTracker rightTracker;
		[ReadOnly]
		public VRTracker hmdTracker;

		[ReadOnly]
		[SerializeField]
		private GameObject _translocationMesh;
		public GameObject translocationMesh {
			get {
				if (!_translocationMesh) {
					if (!currentRigProfile || !currentRigProfile.translocationMeshPrefab) {
						Warning("No translocation mesh prefab has been assigned for this VR Model, or the currentRigProfile hasn't been populated yet.");
						return null;
					}

					_translocationMesh = Instantiate(currentRigProfile.translocationMeshPrefab, Vector3.zero, Quaternion.identity, null);

					VanillaSceneLoader.i.MoveObjectToScene(_translocationMesh, gameObject.scene);
				}

				return _translocationMesh;
			}
			set {
				_translocationMesh = value;
			}
		}

		[Header("VR Mounts")]
		[Tooltip("There are two ways to create or control a mount; either by passing a VRMountConfig asset directly to one of the trackers or by requesting a VRMountConfig by string from the mountDictionary constructed out of these asset files. Depending on the situation, a string or an asset will be more preferable.")]
		public VRMountConfig[] availableMounts = new VRMountConfig[0];

		//public GameObject[] mountPrefabs;
		//public Dictionary<string, VRMountConfig> mountDictionary = new Dictionary<string, VRMountConfig>();
		[Tooltip("This is a list of all the VR tracker mounts that will be available for loading.")]
		public Dictionary<string, VRMountConfig> mountDictionary = new Dictionary<string, VRMountConfig>();

		[Header("VR Interaction System")]

		[Tooltip("If true, objects that inherit from the script InteractorMount or VRInteractable will set their gameObject.layer to the layer matching interactionLayerName on Awake. This is useful since it minimizes the amount of collision-matrix sorting the InteractorMount and VRInteractables need to do.")]
		public bool useCustomInteractionLayer;
		public string interactionLayerName = "VRInteraction";

		[Tooltip("When loaded, a VR rig scene will automatically attach a VanillaCamera script to the object in the rigCamera reference and Initialize it with this nickname. This way, code can safely find the HMD camera by this nickname as soon as its available.")]
		public string hmdCameraNickname = "HMD";
		[ReadOnly]
		public int interactionLayer;

		public UnityEvent onTranslocation = new UnityEvent();

		[Header("Debug")]
		public bool rigOverride;
		public int rigOverrideID;

		public override IEnumerator Initialize() {
			/// - Looking for the correct VR rig scene and SDK

			rigProfileMatchFound = false;

			if (rigOverride) {
				rigProfileMatchFound = true;
				currentRigProfile = modelProfiles[rigOverrideID];
				Log("Rig override has been nominated; the following rig will be loaded instead of running the XRDevice test [{0}]", currentRigProfile.rigScene);
			} else {
				for (int i = 0; i < modelProfiles.Length; i++) {
					if (Equals(XRDevice.model, modelProfiles[i].modelName)) {
						currentRigProfile = modelProfiles[i];
						rigProfileMatchFound = true;
						goto LoopExit;
					}
				}
			}

			LoopExit:

			if (rigProfileMatchFound) {
				Log("An appropriate VR rig [{1}] was found for the loaded XR device [{0}]", XRDevice.model, currentRigProfile.rigScene);
			} else {
				Log("No appropriate VR rig was found for the loaded XR device, if any. We'll be using the default rig as a result [{0}]", currentRigProfile.modelName);
			}

			///	- Add the load instructions

			currentRigProfile.AddLoadInstructions();

			LogXRSettings();

			/// - Reload the SDK if needed

			if (XRSettings.loadedDeviceName == null || !Equals(XRSettings.loadedDeviceName, currentRigProfile.preferredSDK)) {
				Log("A different SDK needs loading to use the current XR device [{0}]", XRDevice.model);

				XRSettings.enabled = false;

				yield return null;

				XRSettings.LoadDeviceByName(currentRigProfile.preferredSDK.ToString());

				yield return null;

				XRSettings.enabled = true;

				yield return null;

				LogXRSettings();
			}

			/// - Populate the mount config dictionary

			for (int i = 0; i < availableMounts.Length; i++) {
				mountDictionary.Add(availableMounts[i].mountPrefab.name, availableMounts[i]);
			}

			/// - Initialize interaction layer

			if (useCustomInteractionLayer) {
				interactionLayer = LayerMask.NameToLayer(interactionLayerName);

				if (interactionLayer == -1) {
					Warning("Using the custom interaction layer for the VR interaction system has been nominated, but no layer by the name [{0}] seems to exist. As a result, interaction consistency is not guaranteed.", interactionLayerName);
					useCustomInteractionLayer = false;
				} else {
					for (int i = 0; i < 31; i++) {
						Physics.IgnoreLayerCollision(interactionLayer, i, true);
					}

					Physics.IgnoreLayerCollision(interactionLayer, interactionLayer, false);
				}
			}

			initialized = true;

			//VanillaSceneLoader.i.activateAutomaticallyAfterSetupCompletion = false;
			//VanillaSceneLoader.i.allowSceneActivationSwitching = false;

			//Log("Running the LoadQueue, which should only contain the VR Rig so far!");

			if (VanillaSceneLoader.i) {
				yield return VanillaSceneLoader.i.RunLoadQueue();

				if (rigScene) {
					ObjectAnchorAutoPose autoPose = rigScene.rigRoot.gameObject.AddComponent<ObjectAnchorAutoPose>();
					autoPose.anchorKey = "VR Rig";
				} else {
					Error("Something has gone wrong with the VR Rig scene-loading process and no viable rig has been loaded.");
				}
			}
		}

		public override void SystemInitializationComplete() {
			Camera systemCam = VanillaCameras.i.GetCamera("System");

			switch (currentRigProfile.preferredSDK) {
				case VRSDK.None:
					VanillaSystemCanvas.i.canvas.planeDistance = 0;
					break;
				case VRSDK.OpenVR:
					VanillaSystemCanvas.i.canvas.planeDistance = 10;

					// This is inexplicable. Having meshes inside the frustum make them not appear, but only in VR (what? Vive only?) so we have to shorten the frustrum.
					systemCam.nearClipPlane = -1;
					systemCam.farClipPlane = 1;
					break;
				case VRSDK.Oculus:
					VanillaSystemCanvas.i.canvas.planeDistance = 10;

					systemCam.nearClipPlane = -1;
					systemCam.farClipPlane = 1;
					break;
			}

			currentRigProfile.CreateVanillaControllers();
		}

		public void LogXRSettings() {
			Log("Current XR SDK status");

			if (XRSettings.enabled) {
				Log("XRSettings is currently enabled.");
			} else {
				Log("XRSettings is currently disabled.");
			}

			Log("XRDevice found [{0}]", XRSettings.isDeviceActive);
			Log("Current SDK [{0}]", XRSettings.loadedDeviceName);
			Log("Current model [{0}]", XRDevice.model);

			Log("The following devices are supported:");

			for (int i = 0; i < XRSettings.supportedDevices.Length; i++) {
				Log(XRSettings.supportedDevices[i]);
			}
		}

		///---------------------------------------------------------------------------------------------------------
		/// VR Tracker Mounts
		///---------------------------------------------------------------------------------------------------------

		public bool MountIsAvailable(string mountName) {
			if (mountDictionary.ContainsKey(mountName)) {
				return true;
			} else {
				Error("The requested VR tracker mount [{0}] hasn't been properly configured for use.", mountName);
				return false;
			}
		}

		public VRMountConfig GetMount(string mountName) {
			if (MountIsAvailable(mountName)) {
				return mountDictionary[mountName];
			} else {
				return null;
			}
		}

		public VRMount CreateMountOnTracker(string mountName, Handedness tracker) {
			switch (tracker) {
				case Handedness.Left:
					return leftTracker.CreateMount(GetMount(mountName));
				case Handedness.Right:
					return rightTracker.CreateMount(GetMount(mountName));
				case Handedness.Middle:
					return hmdTracker.CreateMount(GetMount(mountName));
				default:
					return null;
			}
		}

		public VRMount CreateMountOnTracker(VRMountConfig asset, Handedness tracker) {
			switch (tracker) {
				case Handedness.Left:
					return leftTracker.CreateMount(asset);
				case Handedness.Right:
					return rightTracker.CreateMount(asset);
				case Handedness.Middle:
					return hmdTracker.CreateMount(asset);
				default:
					return null;
			}
		}
	}




	///---------------------------------------------------------------------------------------------------------
	/// VR enums
	///---------------------------------------------------------------------------------------------------------

	public enum VRSDK {
		None,
		OpenVR,
		Oculus
	}

	public enum Handedness {
		Middle = 1,
		Left = 2,
		Right = 4
	}

	public enum TrackerType {
		None,
		Left,
		Head,
		Right
	}

	public enum TranslocatorReflectionType {
		None,
		GroundTagOnly,
		AnyCollider
	}

	public enum TranslocationMeshDisplayMode {
		None,
		ValidSurfacesOnly,
		Always
	}

	[Flags]
	public enum VRInteractionFlags {
		allowInteraction = 1, // Should interaction happen at all?
		allowProximityTracking = 2, // Should proximity calls be processed? (OnInteractorEnter, OnBecameNearest, etc)
		allowTrigger = 4, // Can an interactor use its trigger? This isn't used by an interactable.
		allowGrip = 8, // Can an interactor use the grip trigger? This isn't used by an interactable.
		allowStealing = 16, // Can an interactor take an interactable if its already being interacted with?
	}

	public enum VRPickUpInput {
		Trigger,
		Grip
	}
}