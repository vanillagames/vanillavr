﻿using UnityEngine;

using Vanilla.Inputs;

namespace Vanilla.VR {
	[CreateAssetMenu(fileName = "New VR Mount Config", menuName = "VanillaVR/Mount Config")]
	public class VRMountConfig : ScriptableObject {

		//[Tooltip("This term will be used to find the Mount in the dictionary and should be short and concise.")]
		//public string nickname;

		[Tooltip("The GameObject prefab of the VR Mount. This object will be childed directly to the tracker and shouldn't need to handle any physics tracking code itself. Moreover, it is considered very helpful to have the Prefab 'inactive' by default, create it at a convenient point at runtime and then customize it before enabling. This way, you have a lot more control over how and when your mount appears and functions.")]
		public GameObject mountPrefab;

		[EnumFlags]
		public Handedness trackerCompatability;

		public bool IsCompatibleWithTracker(Handedness handedness) {
			return trackerCompatability.HasFlag(handedness);
		}
	}
}