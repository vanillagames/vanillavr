﻿using UnityEngine;
using System.Collections;

namespace Vanilla.VR {
	public class VRInteractable : VanillaBehaviour {

		[Header("[ VR Interactable ]")]
		[EnumFlags(1)]
		public VRInteractionFlags interactionFlags;

		[ReadOnly]
		[SerializeField]
		private Collider _rangeTrigger;
		public Collider rangeTrigger {
			get {
				if (!_rangeTrigger) {
					_rangeTrigger = GetComponentInChildren<Collider>();
				}

				return _rangeTrigger;
			}
			set {
				_rangeTrigger = value;
			}
		}

		[ReadOnly]
		[SerializeField]
		private Rigidbody _rigidbody;
		public new Rigidbody rigidbody {
			get {
				if (!_rigidbody) {
					_rigidbody = GetComponent<Rigidbody>(); // It has to be attached to receive OnTriggerEnter/Exits

					if (_rigidbody) {
						Log("No rigidbody is attached!");
					}
				}

				return _rigidbody;
			}
			set {
				_rigidbody = value;
			}
		}

		[ReadOnly]
		public VRInteractor currentInteractor;

		public virtual void Awake() {
			if (VanillaVR.i == null) {
				return;
			}

			// If we're using VanillaVRs custom interaction layer (a good idea for optimization!)
			if (VanillaVR.i.useCustomInteractionLayer) {
				// Set whichever object has the rangeTrigger attacheds layer to the custom interaction layer.
				rangeTrigger.gameObject.layer = VanillaVR.i.interactionLayer;
			}
		}

		public void OnInteractorEnter(VRInteractor interactor) {
			if (CanUseProximity()) {
				InteractorEnter(interactor);
			}
		}

		public void OnInteractorExit(VRInteractor interactor) {
			if (CanUseProximity()) {
				InteractorExit(interactor);
			}
		}

		public void OnBecameNearest(VRInteractor interactor) {
			if (CanUseProximity()) {
				InteractorBecameNearest(interactor);
			}
		}

		public void OnNoLongerNearest(VRInteractor interactor) {
			if (CanUseProximity()) {
				InteractorNoLongerNearest(interactor);
			}
		}

		public void OnTriggerClick(VRInteractor interactor) {
			if (CanUseTrigger()) {
				InteractorTriggerClick(interactor);
			}
		}

		public void OnTriggerRelease(VRInteractor interactor) {
			if (CanUseTrigger()) {
				InteractorTriggerRelease(interactor);
			}
		}

		public void OnGripClick(VRInteractor interactor) {
			if (CanUseGrip()) {
				InteractorGripClick(interactor);
			}
		}

		public void OnGripRelease(VRInteractor interactor) {
			if (CanUseGrip()) {
				InteractorGripRelease(interactor);
			}
		}

		/// </summary>

		public virtual void InteractorEnter(VRInteractor interactor) { }

		public virtual void InteractorExit(VRInteractor interactor) { }

		public virtual void InteractorBecameNearest(VRInteractor interactor) { }

		public virtual void InteractorNoLongerNearest(VRInteractor interactor) { }

		public virtual void InteractorTriggerClick(VRInteractor interactor) { }

		public virtual void InteractorTriggerRelease(VRInteractor interactor) { }

		public virtual void InteractorGripClick(VRInteractor interactor) { }

		public virtual void InteractorGripRelease(VRInteractor interactor) { }

		public bool CanUseProximity() {
			return (interactionFlags.HasFlag(VRInteractionFlags.allowInteraction) && interactionFlags.HasFlag(VRInteractionFlags.allowProximityTracking));
		}

		public bool CanUseTrigger() {
			return (interactionFlags.HasFlag(VRInteractionFlags.allowInteraction) && interactionFlags.HasFlag(VRInteractionFlags.allowTrigger)) ;
		}

		public bool CanUseGrip() {
			return (interactionFlags.HasFlag(VRInteractionFlags.allowInteraction) && interactionFlags.HasFlag(VRInteractionFlags.allowGrip));
		}
	}
}