﻿using UnityEngine;

using Vanilla.Inputs;

namespace Vanilla.VR {
	[CreateAssetMenu(fileName = "New VR Model Profile", menuName = "VanillaVR/Model Profile")]
	public class VRModelProfile : ScriptableObject {

		public string modelName;

		public VRSDK preferredSDK;

		public InputMap[] inputMaps;

		public GameObject translocationMeshPrefab;

		[ReadOnly]
		public VanillaSceneAsset rigScene;

		[EnumFlags]
		public Handedness trackersToLoad;

		public void AddLoadInstructions() {
			VanillaLog.Log("Setting up the VR model profile [{0}]", modelName);

			if (VanillaSceneLoader.i == null) {
				VanillaLog.Log("SceneLoader isn't set up; can't do my job.");
				return;
			}

			if (VanillaVR.i == null) {
				VanillaLog.Log("VanillaVR isn't set up; can't do my job.");
				return;
			}

			VanillaSceneLoader.i.AddInstructionToLoadQueue(rigScene, LoadAction.Load);

			if (trackersToLoad.HasFlag(Handedness.Left)) {
				VanillaSceneLoader.i.AddInstructionToLoadQueue(VanillaVR.i.leftTrackerScene, LoadAction.Load);
			}

			if (trackersToLoad.HasFlag(Handedness.Right)) {
				VanillaSceneLoader.i.AddInstructionToLoadQueue(VanillaVR.i.rightTrackerScene, LoadAction.Load);
			}

			if (trackersToLoad.HasFlag(Handedness.Middle)) {
				VanillaSceneLoader.i.AddInstructionToLoadQueue(VanillaVR.i.hmdTrackerScene, LoadAction.Load);
			}
		}

		public void CreateVanillaControllers() {
			for (int i = 0; i < inputMaps.Length; i++) {
				if (inputMaps[i] != null) {
					VanillaInputs.i.CreateNewVanillaControllerFromInputMap(inputMaps[i]);
				} else {
					VanillaLog.Error("This input map reference slot [{0}] is empty.", i);
				}
			}
		}
	}
}