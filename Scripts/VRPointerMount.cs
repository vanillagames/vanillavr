﻿using UnityEngine;
using System.Collections;

namespace Vanilla.VR {
	public class VRPointerMount : VRMount {

		public override void MountSpecificSetup() {
			VRPointer pointer = GetComponent<VRPointer>();

			switch (tracker.handedness) {
				case Handedness.Left:
					break;
				case Handedness.Middle:
					Destroy(pointer.GetComponent<LineRenderer>());


					//pointer.clickUpSubscription.subscribeAutomatically = false;
					//pointer.clickDownSubscription.subscribeAutomatically = false;

					//pointer.clickUpSubscription.Unsubscribe(pointer.UpHandler);
					//pointer.clickDownSubscription.Unsubscribe(pointer.DownHandler);
					break;
				case Handedness.Right:

					break;
			}
		}
	}
}