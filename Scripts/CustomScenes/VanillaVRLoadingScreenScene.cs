﻿using UnityEngine;
using UnityEngine.UI;

using System.Collections;

using Vanilla.Math;
using Vanilla.Transitions;

namespace Vanilla.VR {
	public class VanillaVRLoadingScreenScene : VanillaScene {

		[Header("[ Vanilla VR Loading Screen Scene ]")]

		//public Image logo;
		public Text loadingText;

		Color uiColor;

		public float sinWaveSpeed = 2f;
		public float sinWaveOffset = 0.5f;
		public float sinWaveDepth = 0.475f;

		//public bool fadeLogo;
		public bool fadeText;
		public bool spinBlocks;

		public Transform photoSphere;

		public Transform[] logoBlocks;

		public override void Awake() {
			uiColor = loadingText.color;
			uiColor.a = 0.5f;
		}

		public override IEnumerator Activate() {
			while (VanillaTransitions.i.transitioning) {
				Log("Waiting for another transition to finish...");
				yield return null;
			}

			if (spinBlocks) {
				StartCoroutine("Animate");
			}
		}

		public override void OnEnable() {
			base.OnEnable();

			if (spinBlocks) {
				StartCoroutine("Animate");
			}
		}

		public IEnumerator Animate() {
			while (VanillaSceneLoader.i.setupIsLoading) {
				float i = 0;
				int blockID = Random.Range(0, logoBlocks.Length - 1);

				Vector3 from = logoBlocks[blockID].transform.localEulerAngles;
				Vector3 to = from;

				switch (Random.Range(0, 2)) {
					case 0:
						to = Random.value < 0.5f ? new Vector3(90, 0, 0) : new Vector3(-90, 0, 0);
						//to += new Vector3(90, 0, 0);
						break;
					case 1:
						to += Random.value < 0.5f ? new Vector3(0, 90, 0) : new Vector3(0, -90, 0);
						//to += new Vector3(0, 90, 0);
						break;
					case 2:
						to += Random.value < 0.5f ? new Vector3(0, 0, 90) : new Vector3(0, 0, -90);
						//to += new Vector3(0, 0, 90);
						break;
				}

				while (i < 1.0f) {
					i += Time.deltaTime;

					//logoBlocks[blockID].transform.localEulerAngles = Vector3.LerpUnclamped(to, from, VanillaMath.Hermite(i));
					logoBlocks[blockID].transform.localEulerAngles = Vector3.LerpUnclamped(to, from, VanillaMath.Sinerp(i));

					yield return null;
				}

				//yield return Vanilla.waitForSeconds[1];

				yield return null;
			}
		}

		public void Update() {
			//photoSphere.position = VanillaVR.i.rigScene.rigCamera.position;

			uiColor.a = sinWaveOffset + (VanillaMath.SinWave(sinWaveSpeed) * sinWaveDepth);

			//if (fadeLogo) {
			//	logo.color = uiColor;
			//}

			if (fadeText) {
				loadingText.color = uiColor;
			}
		}

		public override IEnumerator SceneUnloaded() {
			while (VanillaTransitions.i.transitioning) {
				Log("Waiting for another transition to finish...");
				yield return null;
			}

			if (spinBlocks) {
				StopCoroutine("Animate");
			}
		}
	}
}