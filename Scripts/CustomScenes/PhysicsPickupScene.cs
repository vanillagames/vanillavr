﻿using UnityEngine;
using UnityEngine.UI;

using System.Collections;

using Vanilla.Transitions;

namespace Vanilla.VR {
	public class PhysicsPickupScene : VRTestRoomScene {

		[Header("[ Physics Pick-up Example ]")]

		[ReadOnly]
		public Vector3 defaultGravity;
		[ReadOnly]
		public Vector3 gravityCache;

		public override IEnumerator SceneLoaded() {
			defaultGravity = Physics.gravity;
			gravityCache = defaultGravity;
			yield break;
		}

		public override IEnumerator Activate() {
			yield return base.Activate();
		}

		public void TurnGravityOff() {
			Physics.gravity = Vector3.zero;
		}

		public void TurnGravityOn() {
			Physics.gravity = gravityCache;
		}

		public void ChangeGravity(Vector3 newGravity) {
			gravityCache = newGravity;
			Physics.gravity = gravityCache;
		}

		public void ResetGravity() {
			gravityCache = defaultGravity;
		}
	}
}