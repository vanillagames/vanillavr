﻿using UnityEngine;
using System.Collections;

namespace Vanilla.VR {
	public class VRTestRoomScene : LeadScene {

		[Header("[ VR Test Room Scene ]")]

		public VRMountConfig[] autoCreateLeftMounts;
		public VRMountConfig[] autoCreateRightMounts;
		public VRMountConfig[] autoCreateHMDMounts;

		public override IEnumerator Activate() {
			yield return base.Activate();

			if (!VanillaVR.i) {
				Warning("VanillaVR is currently unavailable.");
				yield break;
			}

			for (int i = 0; i < autoCreateLeftMounts.Length; i++) {
				VanillaVR.i.leftTracker.CreateMount(autoCreateLeftMounts[i]);
			}

			for (int i = 0; i < autoCreateRightMounts.Length; i++) {
				VanillaVR.i.rightTracker.CreateMount(autoCreateRightMounts[i]);
			}

			for (int i = 0; i < autoCreateHMDMounts.Length; i++) {
				VanillaVR.i.hmdTracker.CreateMount(autoCreateHMDMounts[i]);
			}

			yield break;
		}
	}
}