﻿using UnityEngine;
using System.Collections;

namespace Vanilla.VR {
	public class Translocation : VRTestRoomScene {

		[Header("[ Translocation ]")]
		[ReadOnly]
		public bool[] platformSuccesses;

		public Color successfulTranslocationColor;

		public AudioClip victory;

		public AudioSource speaker;

		VRTranslocator translocator;

		public VRMountConfig translocatorAsset;

		public GameObject[] platforms;
		Material[] platformMaterials;

		public int successCount;

		public override IEnumerator Activate() {
			platforms = GameObject.FindGameObjectsWithTag("Ground");
			platformSuccesses = new bool[platforms.Length];

			platformMaterials = new Material[2];

			platformMaterials[0] = new Material(platforms[0].GetComponent<MeshRenderer>().material);

			platformMaterials[1] = new Material(platformMaterials[0]);
			platformMaterials[1].SetColor("_Tint", successfulTranslocationColor);

			yield return base.Activate();

			translocator = VanillaVR.i.rightTracker.CreateMount(translocatorAsset) as VRTranslocator;

			translocator.onTranslocation.AddListener(OnTranslocation);
		}

		public void OnTranslocation(GameObject translocatedSurface) {
			//translocatedSurface = translocator.currentHit.gameObject;

			translocatedSurface.GetComponent<MeshRenderer>().material = platformMaterials[1];

			for (int i = 0; i < platforms.Length; i++) {
				if (platforms[i] == translocatedSurface && !platformSuccesses[i]) {
					platformSuccesses[i] = true;
					successCount++;

					speaker.Play();
				}
			}

			if (successCount == platformSuccesses.Length) {
				Invoke("Victory", 1.0f);
				successCount++;
			}
		}

		public void Victory() {
			speaker.PlayOneShot(victory);
		}
	}
}