﻿using UnityEngine;
using System.Collections;

namespace Vanilla.VR {
	public class VRTranslocationMesh : VanillaBehaviour {

		public Material[] paintableMaterials;

		[ReadOnly]
		public VRTranslocator translocator;

		public AudioSource speaker;

		public AudioClip translocationAccepted;
		public AudioClip translocationDenied;

		// This is called when the translocator OnEnables
		public virtual void TranslocatorEnabled(VRTranslocator t) {
			translocator = t;

			Activate();
		}

		// This is called when the translocator OnDisables
		public virtual void TranslocatorDisabled() {
			Deactivate();
		}

		public virtual void Activate() {
			gameObject.SetActive(true);
		}

		public virtual void Deactivate() {
			gameObject.SetActive(false);
		}

		public virtual void TranslocationBecameValid(Color c) {
			Recolor(c);
		}

		public virtual void TranslocationBecameInvalid(Color c) {
			Recolor(c);
		}

		public void Move(Vector3 newPosition) {
			transform.position = newPosition;
		}

		// This is invoked when translocation occurs.
		public virtual void Translocate() {
			if (speaker) {
				speaker.PlayOneShot(translocationAccepted);
			}
		}

		// This is invoked if the trigger is clicked but translocation is not currently allowed.
		public virtual void TranslocateDenied() {
			if (speaker) {
				speaker.PlayOneShot(translocationDenied);
			}
		}

		// This is invoked if/when assets need to be repainted
		public virtual void Recolor(Color c) {
			for (int i = 0; i < paintableMaterials.Length; i++) {
				paintableMaterials[i].color = c;
			}
		}
	}
}