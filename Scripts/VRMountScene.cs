﻿//using UnityEngine;
//using UnityEngine.SceneManagement;

//using System.Collections;

//namespace Vanilla.VR {
//	public class VRMountScene : VanillaScene {

//		public GameObject mountPrefab;

//		public bool availableForLeftHand;
//		public bool availableForRightHand;
//		public bool availableForHMD;

//		[ReadOnly]
//		public VRMount leftMount;
//		[ReadOnly]
//		public VRMount rightMount;
//		[ReadOnly]
//		public VRMount hmdMount;

//		public override IEnumerator SceneLoad () {
//			// This needs a good rewrite; the terminology is getting weird and overlapping.
//			// The way this needs to work is remarkably similar for both the Tracker and the TrackerMounts.
//			// - Both need to have the option to be Fixed or Chase based
//			// - Both need to have a target
//			// - Both need to reset to their targets pose at OnSceneLoadSync (Tracker first, then TrackerMounts)
//			// Besides that, they aren't really that different in terms of functionality. Surely this could be consolidated?
//			// In the mean-time, you're just manually setting the position of the Mounts to the rig transforms, which is technically ideal.
//			// But just so you know, don't take whats here as gospel. Ideally, both Tracker and Mount would be reset to the exact rig transform poses
//			// But know to 'follow' distinct targets. In other words...

//			/* OnEnable - Left/Right/HMD Tracker subscribes 'ResetPose' to OnSceneLoadSync
//			/* OnEnable - Left/Right/HMD TrackerMount subscribes 'ResetPose' to OnSceneLoadSync
//			 * 
//			 * OnSceneLoadSync - Left/Right/HMD Tracker sets its pose to Left/Right/HMD TrackerTarget pose.
//			 * OnSceneLoadSync - Left/Right/HMD TrackerMount sets its pose to Left/Right/HMD TrackerTarget pose.
//			 * 
//			 * OnUpdate - Left/Right/HMD Tracker moves towards or handles its attachment to Left/Right/HMD TrackerTarget
//			 * OnUpdate - Left/Right/HMD TrackerMount moves towards or handles its attachment to Left/Right/HMD Tracker
//			 * 
//			 * Maybe to make this clearer, the names could simply be RigTarget/RigTracker/RigMount?
//			 */

//			if (availableForLeftHand && VanillaVR.i.leftTracker) {
//				leftMount = CreateNewMountObject(Handedness.Left);
//			}

//			if (availableForRightHand && VanillaVR.i.rightTracker) {
//				rightMount = CreateNewMountObject(Handedness.Right);
//			}

//			if (availableForHMD && VanillaVR.i.hmdTracker) {
//				hmdMount = CreateNewMountObject(Handedness.None);
//			}

//			yield return null;
//		}

//		VRMount CreateNewMountObject(Handedness handedness) {
//			VRMount newMount = Instantiate(mountPrefab).GetComponent<VRMount>();

//			VanillaSceneLoader.i.MoveObjectToScene(newMount.gameObject, gameObject.scene);

//			newMount.TrackerSpecificSetup(handedness);

//			return newMount;
//		}
//	}
//}