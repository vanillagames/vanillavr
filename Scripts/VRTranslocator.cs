﻿using UnityEngine;
using UnityEngine.Events;

using System.Collections;
using System.Collections.Generic;

using Vanilla.Inputs;
using Vanilla.Transitions;
using Vanilla.CustomEvents;

namespace Vanilla.VR {
	[System.Serializable]
	[RequireComponent(typeof(LineRenderer))]
	public class VRTranslocator : VRMount {

		[Header("[ VR Translocator ]")]
		[Header("Settings")]
		[Tooltip("How long in world units should each segment of the ray parabola be?")]
		public float raySegmentLength = 0.333f;

		[Tooltip("Scale the size of the entire parabola")]
		[SerializeField]
		public float parabolaScalarInput;

		[SerializeField]
		public float parabolaScalar = 0.00025f;

		[SerializeField]
		private float parabolaScalarMin = 0.000125f;

		[SerializeField]
		private float parabolaScalarMax = 0.0005f;

		[SerializeField]
		private float parabolaScalarSensitivity = 1.0f;

		[SerializeField]
		private float _parabolaScalarNormal = 0.5f;
		public float parabolaScalarLerp {
			get {
				return _parabolaScalarNormal;
			}
			set {
				_parabolaScalarNormal = Mathf.Clamp01(value);

				parabolaScalar = Mathf.Lerp(parabolaScalarMin, parabolaScalarMax, _parabolaScalarNormal);
			}
		}

		[Tooltip("maxMagnitudeDelta used in RotateTowards. This doesn't seem to make any difference, however.")]
		public float maxMagnitudeDelta = 1.0f;

		//[Tooltip("If the dot product of a hit normal from a ray segment falls short of the 'successAngleTolerance' range but within the 'reflectAngleTolerance' range, should we bounce the direction of the ray on the x and z axis?.")]
		//public bool reflectRaysOffWalls = true;

		[Tooltip("In what way should the translocator ray reflect off surfaces?")]
		public TranslocatorReflectionType reflectionMode = TranslocatorReflectionType.GroundTagOnly;

		[Tooltip("How much tolerance should we have for allowing translocation to a tilted surface?\n-1 would allow the user to translocate to any surface.\n1 would only allow perfectly flat surfaces.")]
		[Range(-1, 1)]
		public float successAngleTolerance = 1.0f;

		[Tooltip("How much tolerance should we have for reflecting the ray parabolas direction off walls? This needs to be below successAngleTolerance, and any angle lower than this will be rejected.")]
		[Range(-0.75f, 0.75f)]
		public float reflectAngleTolerance = -0.25f;

		[Tooltip("How far from a reflected surface should the added line renderer point be? This is set automatically on Awake to match half the width of the line renderer itself.")]
		//[ReadOnly]
		public float reflectionLinePointOffset;

		[Tooltip("The maximum number of ray segments that can be cast in a single frame.")]
		public int raySegmentMaximum = 50;

		[Tooltip("If true, the translocator visualizations will be recoloured to the below 'valid' or 'invalid' colors respectively.")]
		public bool doRecolouring;

		public Color validColour;
		public Color invalidColour;

		[Tooltip("How fast should the line offset animation run?")]
		public float offsetAnimationSpeed = 0.5f;

		//[Header("Translocation Point")]
		//public Transform translocationPoint;

		[ReadOnly]
		[SerializeField]
		private VRTranslocationMesh translocationMesh;

		[ReadOnly]
		[SerializeField]
		private bool _rayRanOutOfSegments;
		public bool rayRanOutOfSegments {
			get {
				return _rayRanOutOfSegments;
			}
			set {
				if (!_rayRanOutOfSegments) {
					if (value) {
						translocationMesh.Deactivate();
					}
				} else if (!value) {
					translocationMesh.Activate();
				}

				_rayRanOutOfSegments = value;
			}
		}

		[Header("Input")]
		//[SerializeField]
		//public InputEventSubscription clickSubscription;
		//public InputEventSubscription unclickSubscription;
		//public InputEventSubscription rangeSubscription;

		//public ProjectAction translocate;
		//public ProjectAction translocateExtension;

		[Header("Status")]
		//[ReadOnly]
		//public int currentRaySegment;

		[ReadOnly]
		[Tooltip("Did the parabola land on any surface?")]
		public float angleBetweenForwardAndUp;

		[ReadOnly]
		[Tooltip("Did the parabola land on any surface?")]
		public bool rayHitSomething;

		[ReadOnly]
		[Tooltip("Which valid object did we land on?")]
		public Collider currentHit;

		[HideInInspector]
		[Tooltip("The points to be fed into the line renderer")]
		public List<Vector3> lineRendererPoints = new List<Vector3>();

		[Tooltip("When this is true, we no longer add to the ray parabola")]
		bool keepAddingToParabola;

		float lineMaterialOffset;

		[Tooltip("Did the parabola land on an eligible surface?")]
		[SerializeField]
		private bool _translocationValid;
		public bool translocationValid {
			get {
				return _translocationValid;
			}
			set {
				if (_translocationValid) {
					if (!value) {
						// Translocator just became invalid
						RecolourAssets(false);

						translocationMesh.TranslocationBecameInvalid(invalidColour);
					}
				} else if (value) {
					// Translocator just became valid
					RecolourAssets(true);

					translocationMesh.TranslocationBecameValid(validColour);
				}

				_translocationValid = value;
			}
		}

		[Header("Translocation")]
		[ReadOnly]
		public bool translocating;

		[ReadOnly]
		public Vector3 translocationPosition;

		public TransitionConfiguration preTranslocation;
		public TransitionConfiguration postTranslocation;

		public GameObjectEvent onTranslocation = new GameObjectEvent();

		[Header("Debug")]
		public bool breakAtTheEndOfEachRay;
		public bool breakAtTheEndOfEachFrame;

		RaycastHit hit;
		LineRenderer lineRenderer;

		Vector3 rayDir;
		Vector3 rayOrigin;

		Material lineRendererMaterial;

		// [ToDo] - fix the weird glitch with the last point. It's very wonky if you reflect off a non-translocatable surface.
		
		public override void MountSpecificSetup() {
			lineRenderer = GetComponent<LineRenderer>();
			lineRendererMaterial = new Material(lineRenderer.material);
			lineRenderer.material = lineRendererMaterial;

			reflectionLinePointOffset = lineRenderer.widthMultiplier * 0.5f;


			if (VanillaVR.i && VanillaVR.i.translocationMesh) {
				translocationMesh = VanillaVR.i.translocationMesh.GetComponent<VRTranslocationMesh>();
			}

			if (doRecolouring) {
				lineRendererMaterial.color = validColour;
			}
		}

		//public override void TrackerSpecificSetup(int newInputID) {
		//}

		public void OnEnable() {

			//clickSubscription.inputID = unclickSubscription.inputID = rangeSubscription.inputID = tracker ? tracker.inputID : 0;

			if (translocationMesh) {
				translocationMesh.TranslocatorEnabled(this);
			}

			VanillaInputs.i.Subscribe(ProjectAction.Translocate, HandleClick);
			VanillaInputs.i.Subscribe(ProjectAction.TranslocatorExtend, HandleNewRange);

			//clickSubscription.Subscribe(HandleClick);
			//unclickSubscription.Subscribe(HandleUnclick);
			//rangeSubscription.Subscribe(HandleNewRange);
		}

		public void HandleClick() {
			if (!translocating && translocationValid) {
				currentHit = hit.collider;

				StartCoroutine("Translocate");
			}
		}

		IEnumerator Translocate() {
			translocating = true;

			translocationPosition = hit.point;

			yield return VanillaTransitions.i.Animate(preTranslocation);

			VanillaVR.i.rigScene.rigRoot.position = translocationPosition;

			tracker.ResetTrackerPositions();

			ResetTrackerMountPosition();

			onTranslocation.Invoke(currentHit.gameObject);

			VanillaVR.i.onTranslocation.Invoke();

			yield return VanillaTransitions.i.Animate(postTranslocation);

			translocating = false;
		}

		public void HandleUnclick() {
			//Log("You let go as well!");
		}

		public void HandleNewRange(float newValue) {
			//Error("No really!");

			parabolaScalarInput = newValue;

			parabolaScalarLerp = (0.5f + (newValue * 0.5f)) * parabolaScalarSensitivity;
			//Log("New Value [{0}]",newValue);

			//parabolaScalar = Mathf.Lerp(parabolaScalarMin, parabolaScalarMax, parabolaScalarLerp);
		}

		void OnDisable() {
			if (translocationMesh) {
				translocationMesh.TranslocatorDisabled();
			}

			VanillaInputs.i.Unsubscribe(ProjectAction.Translocate, HandleClick);
			VanillaInputs.i.Unsubscribe(ProjectAction.TranslocatorExtend, HandleNewRange);

			//clickSubscription.Unsubscribe(HandleClick);
			//unclickSubscription.Unsubscribe(HandleUnclick);
			//rangeSubscription.Unsubscribe(HandleNewRange);
		}

		void Update() {
			RunRayParabola();
			Animate();
		}

		void RunRayParabola() {
			//Log("Beginning ray parabola");

			rayDir = transform.forward;
			rayOrigin = transform.position;

			angleBetweenForwardAndUp = 180 - Vector3.Angle(rayDir, Vector3.up);

			//originalAngleNormal = Mathf.Clamp01((Vector3.Angle(rayDir, Vector3.down) - 90) / 90);

			lineRendererPoints.Clear();

			keepAddingToParabola = true;

			//currentRaySegment = -1;

			while (keepAddingToParabola) {
				if (lineRendererPoints.Count < raySegmentMaximum) {
					CastRaySegment();
				} else {
					// If we go over the allowed number of raySegments...
					keepAddingToParabola = false;

					translocationValid = false;

					lineRendererPoints.Add(rayOrigin + rayDir);

					rayRanOutOfSegments = true;
				}
			}

			// Shouldn't this just happen anyway?
			if (rayHitSomething) {
				lineRendererPoints.Add(hit.point);
				//translocationPoint.position = hit.point;

				//VanillaVR.i.rigScene.translocationMesh?.transform.position = hit.point;
			}

			//currentRaySegment++;

			lineRenderer.positionCount = lineRendererPoints.Count;
			lineRenderer.SetPositions(lineRendererPoints.ToArray());

			// [ToDo] - Check if InputEvents resolve before or after Update here.
			// It needs to be after so a trigger click event can happen after we know if translocation is valid this frame.

			if (breakAtTheEndOfEachFrame) {
				Debug.Break();
			}
		}

		void CastRaySegment() {
			//Log("Running ray segment [" + lineRendererPoints.Count + "] RayOrigin [" + rayOrigin.ToString() + "] RayDir [" + rayDir.ToString() + "]");

			lineRendererPoints.Add(rayOrigin);

			Physics.Raycast(rayOrigin, rayDir, out hit, raySegmentLength);

			rayHitSomething = hit.collider;

			if (rayHitSomething) {
				float hitAngleDotProduct = Vector3.Dot(hit.normal, Vector3.up);

				if (hit.collider.CompareTag("Ground")) {
					if (hitAngleDotProduct >= successAngleTolerance) {
						// The arc segment connected with a collider tagged as 'ground' and the angle of the polygon that it hit is within our designated angle tolerance.
						//Log("Hit the ground and its angle is in the success range!");

						keepAddingToParabola = false;

						//translocationPoint.rotation = Quaternion.Euler(hit.normal);

						translocationValid = true;

						rayRanOutOfSegments = false;

						MoveTranslocationMesh();

						lineRendererPoints.Add(hit.point);
					} else if (reflectionMode != TranslocatorReflectionType.None && hitAngleDotProduct >= reflectAngleTolerance) {
						// The arc segment connected with a collider tagged as 'ground', but the angle of the surface is within the 'reflect' tolerance range.
						//Log("Hit the ground and its angle is in the reflect range");

						HandleRayReflection();
					} else {
						// The arc segment connected with a collider tagged as 'ground', but the angle of the polygon that it hit is outside of our allowed angle tolerance.
						//Log("Hit the ground but its angle is neither successful or reflectable");

						keepAddingToParabola = false;

						//translocationPoint.rotation = Quaternion.Euler(hit.normal);

						translocationValid = false;

						rayRanOutOfSegments = false;

						MoveTranslocationMesh();

						lineRendererPoints.Add(hit.point);
					}
				} else {
					if (reflectionMode == TranslocatorReflectionType.AnyCollider && hitAngleDotProduct < successAngleTolerance && hitAngleDotProduct > reflectAngleTolerance) {
						// The arc segment connected with a collider not tagged as 'ground', but the angle of the surface is within the 'reflect' tolerance range.
						//Log("Hit something that isn't the ground, but its angle is in the reflect range");

						HandleRayReflection();
					} else {
						// The arc segment connected with a collider but the collider isn't tagged as 'ground' and it's not within the success or reflect margins

						keepAddingToParabola = false;

						translocationValid = false;

						rayRanOutOfSegments = false;

						MoveTranslocationMesh();

						lineRendererPoints.Add(hit.point);
					}
				}
			} else {
				// This arc segment didn't hit any colliders.
				//Log("Hit nothing");

				TiltRayDirectionDown();

				rayOrigin += rayDir * raySegmentLength;
			}

			if (breakAtTheEndOfEachRay) {
				Debug.Break();
			}
		}

		void HandleRayReflection() {
			lineRendererPoints.Add(hit.point);

			float remainingSegmentLength = raySegmentLength - Vector3.Distance(rayOrigin, hit.point);

			ReverseRayDirection();

			rayOrigin = hit.point + (rayDir * (remainingSegmentLength));

			/////-----

			//float distToSurface = Vector3.Distance(rayOrigin, hit.point);

			//rayOrigin = hit.point + (-rayDir * (raySegmentLength - reflectionLinePointOffset));

			//rayOrigin = hit.point - (rayDir * reflectionLinePointOffset);

			//lineRendererPoints.Add(rayOrigin);

			//float remainingSegmentLength = raySegmentLength - Vector3.Distance(rayOrigin, hit.point);

			//ReverseRayDirection();

			//rayOrigin = hit.point + (rayDir * (remainingSegmentLength));
		}

		void ReverseRayDirection() {
			float y = rayDir.y;

			rayDir = Vector3.Reflect(rayDir, hit.normal);

			rayDir.y = y;
		}

		void TiltRayDirectionDown() {
			rayDir = Vector3.RotateTowards(rayDir, -Vector3.up, angleBetweenForwardAndUp * parabolaScalar, maxMagnitudeDelta);
		}

		void MoveTranslocationMesh() {
			if (translocationMesh) {
				translocationMesh.Move(hit.point);
			}
		}

		void RecolourAssets(bool valid) {
			Log("Repainting things - valid? [{0}]", valid);

			if (valid) {
				lineRendererMaterial.color = validColour;

				if (translocationMesh) {
					translocationMesh.Recolor(validColour);
				}
			} else {
				lineRendererMaterial.color = invalidColour;

				if (translocationMesh) {
					translocationMesh.Recolor(invalidColour);
				}
			}
		}

		void Animate() {
			lineRendererMaterial.SetTextureOffset("_MainTex", new Vector2(Mathf.Repeat(lineMaterialOffset += (Time.deltaTime * offsetAnimationSpeed), 1.0f), 0));

			//if (translocationMesh) {
			//	if (translocationValid) {
			//		//translocationMesh?.ValidTranslocationFrame();
			//	} else {
			//		//translocationMesh?.InvalidTranslocationFrame();
			//	}
			//}
		}
	}
}