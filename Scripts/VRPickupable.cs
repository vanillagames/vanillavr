﻿using UnityEngine;
using UnityEngine.Events;

using System.Collections;

namespace Vanilla.VR {
	public class VRPickupable : VRInteractable {

		[Header("[ VR Pickupable ]")]
		public UnityEvent onPickUp = new UnityEvent();
		public UnityEvent onRelease = new UnityEvent();

		public VRPickUpInput pickupInputType;

		private FixedJoint _fixedJoint;
		public FixedJoint fixedJoint {
			get {
				if (!_fixedJoint) {
					_fixedJoint = gameObject.AddComponent<FixedJoint>();
				}

				return _fixedJoint;
			}
		}

		//public override void Awake() {
		//	base.Awake();
		//}

		public override void InteractorTriggerClick(VRInteractor interactor) {
			Log("If this interactable is set to not be able to be triggered and you're still seeing this message, it means that the base function above doesn't actually return the whole function, only the base function, and then proceeding. That would be pretty annoying, and you'll need to think of a way around that situation.");

			if (pickupInputType == VRPickUpInput.Trigger) {
				CheckIfPickupIsValid(interactor);
			}
		}

		public override void InteractorGripClick(VRInteractor interactor) {
			if (pickupInputType == VRPickUpInput.Grip) {
				CheckIfPickupIsValid(interactor);
			}
		}

		public override void InteractorTriggerRelease(VRInteractor interactor) {
			Log("If this interactable is set to not be able to be triggered and you're still seeing this message, it means that the base function above doesn't actually return the whole function, only the base function, and then proceeding. That would be pretty annoying, and you'll need to think of a way around that situation.");

			if (pickupInputType == VRPickUpInput.Trigger && currentInteractor && currentInteractor == interactor) {
				Released();
			}
		}

		public override void InteractorGripRelease(VRInteractor interactor) {
			if (pickupInputType == VRPickUpInput.Grip && currentInteractor && currentInteractor == interactor) {
				Released();
			}
		}

		void CheckIfPickupIsValid(VRInteractor interactor) {
			// If this interactable is currently being interacted with by a different interactor
			if (currentInteractor && currentInteractor != interactor) {
				// And either the current interactor doesn't allow stealing or this particular interactable doesn't allow stealing
				if (!currentInteractor.interactionFlags.HasFlag(VRInteractionFlags.allowStealing) || !interactionFlags.HasFlag(VRInteractionFlags.allowStealing)) {
					// Don't do anything.
					return;
				}

				// Otherwise, if stealing is allowed, release this from its current interactor.
				Released();
			}

			currentInteractor = interactor;

			PickedUp();
		}
		
		public virtual void PickedUp() {
			fixedJoint.connectedBody = currentInteractor.rigidbody;
		}

		public virtual void Released() {
			Destroy(fixedJoint);
		}
	}
}