﻿using UnityEngine;

using System.Collections.Generic;

using Vanilla.Inputs;
using System.Collections;

namespace Vanilla.VR {
	public class VRRigScene : VanillaScene {

		[Header("[ VR Rig Scene ]")]

		// Each rig controller needs a different offset position and rotation for the WorldCanvasPointers, since they don't all seem to have a sensible center.
		// To combat this, have a childed transform on each rig controller that you want to act as the real center and we can tell the WorldCanvasPointers to get their position/rotation from them instead.
		// Note that this code is not implemented in the WorldCanvasPointer script yet.
		[SerializeField]
		private Transform _leftTrackerTarget;
		public Transform leftTrackerTarget {
			get {
				if (!_leftTrackerTarget) {
					Error("The left controller has been requested but it's not populated. Please set the reference for the left controller first!");
				}

				return _leftTrackerTarget;
			}
			set {
				_leftTrackerTarget = value;
			}
		}

		[SerializeField]
		private Transform _rightTrackerTarget;
		public Transform rightTrackerTarget {
			get {
				if (!_rightTrackerTarget) {
					Error("The right controller has been requested but it's not populated. Please set the reference for the right controller first!");
				}

				return _rightTrackerTarget;
			}
			set {
				_rightTrackerTarget = value;
			}
		}

		[SerializeField]
		private Transform _hmdTrackerTarget;
		public Transform hmdTrackerTarget {
			get {
				if (!_hmdTrackerTarget) {
					Error("The HMD tracker has been requested but it's not populated. Please set the reference for the right controller first!");
				}

				return _hmdTrackerTarget;
			}
			set {
				_hmdTrackerTarget = value;
			}
		}

		[SerializeField]
		private Transform _rigRoot;
		public Transform rigRoot {
			get {
				if (!_rigRoot) {
					Error("The HMD tracker has been requested but it's not populated. Please set the reference for the right controller first!");
				}

				return _rigRoot;
			}
			set {
				_rigRoot = value;
			}
		}

		[SerializeField]
		private Transform _rigCamera;
		public Transform rigCamera {
			get {
				if (!_rigCamera) {
					Error("The HMD tracker has been requested but it's not populated. Please set the reference for the right controller first!");
				}

				return _rigCamera;
			}
			set {
				_rigCamera = value;
			}
		}

		//public PointerType UIPointer;

		//public PointerType[] otherPointers = new PointerType[1];

		public override void OnEnable() {
			base.OnEnable();

			VanillaVR.i.rigScene = this;
		}

		public override IEnumerator SceneLoaded() {
			if (!VanillaVR.i) {
				Error("No VanillaVR singleton found, so I can't assign myself.");
				yield return null;
			}

			if (rigCamera) {
				VanillaCamera VanillaHMDCam = rigCamera.gameObject.AddComponent<VanillaCamera>();

				string hmdNickname = VanillaVR.i.hmdCameraNickname;

				if (string.IsNullOrEmpty(hmdNickname)) {
					hmdNickname = "HMD";
				}

				VanillaHMDCam.cameraNickname = hmdNickname;
				VanillaHMDCam.Initialize();
			}

			//VanillaCameras.i.GetCamera("System").transform.SetParent(VanillaVR.i.rigScene.rigCamera, false);

			//VanillaVR.i.RigSceneLoaded(leftTracker, rightTracker, hmdTracker);
		}

		//public override IEnumerator SceneLoadSync() {
		//VanillaLoadingScreens.i.GetLoadingScreen(1).transform.SetParent(VanillaVR.i.rigScene.rigCamera, false);

		//yield return null;
		//}

		public override IEnumerator SceneUnloaded() {
			if (VanillaVR.i) {
				VanillaVR.i.rigScene = null;
				yield return null;
			}
		}

		/*
		public void LoadVRPointers() {
			for (int i = 0; i < otherPointers.Length; i++) {
				LoadPointer(otherPointers[i]);
			}

			LoadPointer(UIPointer);
		}

		public void LoadPointer(PointerType whichPointer) {
			switch (whichPointer) {
				case PointerType.Left:
					if (!VanillaSceneLoader.i.SceneIsLoaded("LeftHandPointer")) {
						VanillaSceneLoader.i.LazyLoad("LeftHandPointer");
					}
					break;
				case PointerType.Head:
					if (!VanillaSceneLoader.i.SceneIsLoaded("HMDPointer")) {
						VanillaSceneLoader.i.LazyLoad("HMDPointer");
					}
					break;
				case PointerType.Right:
					if (!VanillaSceneLoader.i.SceneIsLoaded("RightHandPointer")) {
						VanillaSceneLoader.i.LazyLoad("RightHandPointer");
					}
					break;
			}
		}
		*/
	}
}