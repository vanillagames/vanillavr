﻿using UnityEngine;
using System.Collections;

using Vanilla.Math;

namespace Vanilla.VR {
	public class TestRoomBlocks : VRInteractable {

		[Header("[ Test Room Block ]")]

		public new Light light;

		const float lightUpSpeed = 1.0f;
		const float fadeOutSpeed = 0.5f;

		float defaultIntensity;
		public Color defaultEmissiveColor;
		public Color transitionColor;

		public bool interactorNearby;
		public float FXnormal;

		MeshRenderer meshRenderer;

		void Start() {
			FXnormal = 0;
			meshRenderer = GetComponent<MeshRenderer>();

			meshRenderer.material = new Material(meshRenderer.material);

			defaultEmissiveColor = meshRenderer.material.GetColor("_EmissionColor");

			meshRenderer.material.SetColor("_EmissionColor", Color.black);
			transitionColor = Color.black;

			defaultIntensity = light.intensity;
			light.intensity = 0;
			enabled = false;
		}

		public override void InteractorBecameNearest(VRInteractor interactor) {
			enabled = true;
			interactorNearby = true;
		}

		public override void InteractorNoLongerNearest(VRInteractor interactor) {
			interactorNearby = false;
		}

		void Update() {
			float intensity = light.intensity;

			FXnormal = interactorNearby ? Mathf.Clamp01(FXnormal + (Time.deltaTime * lightUpSpeed)) : Mathf.Clamp01(FXnormal - (Time.deltaTime * fadeOutSpeed));

			float n = VanillaMath.Sinerp(FXnormal);

			intensity = Mathf.Lerp(0, defaultIntensity, n);
			transitionColor = Color.Lerp(Color.black, defaultEmissiveColor, n);

			//if (interactorNearby) {
			//	intensity = Mathf.Lerp(intensity, defaultIntensity, FXnormal);
			//	transitionColor = Color.Lerp(colorTemp, defaultEmissiveColor, n);
			//} else {
			//	intensity = Mathf.Lerp(intensity, 0, FXnormal);
			//	colorTemp = Color.Lerp(colorTemp, Color.black, FXnormal);

			if (FXnormal < 0.001f) {
				FXnormal = 0;
				enabled = false;
			}
			//}

			light.intensity = intensity;
			meshRenderer.material.SetColor("_EmissionColor", transitionColor);
		}
	}
}