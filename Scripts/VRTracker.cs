﻿using UnityEngine;

using System.Collections.Generic;

using Vanilla.Inputs;
using Vanilla.ThreeDee;

namespace Vanilla.VR {
#if UNITY_EDITOR
	using UnityEditor;


	[CustomEditor(typeof(VRTracker))]  
	public class VRTrackerEditor : VanillaEditor {

		VRTracker tracker;

		void OnEnable() {
			tracker = (VRTracker)behaviour;
		}

		public override void OnInspectorGUI() {
			base.OnInspectorGUI();

			if (GUILayout.Button("Create Mount")) {
				tracker.CreateMount(tracker.debugMountConfig);
			}

			if (GUILayout.Button("Destroy Mount")) {
				tracker.DestroyMount(tracker.debugMountConfig);
			}

			 
		}
	}
#endif


	public class VRTracker : VanillaController {

		[Header("[ VR Tracker ]")]

		//[ReadOnly]
		public Handedness handedness;

		public int inputID;

		//public List<VRTrackerMountScene> currentMounts;

		public PhysicsChaseTarget physicsChase;
		public PhysicsRotateToMatchTarget physicsRotate;

		public Transform target;

		public VRMountConfig debugMountConfig;

		[Tooltip("This is a dictionary of all the currently loaded mounts attached to this tracker.")]
		public Dictionary<VRMountConfig, VRMount> mounts = new Dictionary<VRMountConfig, VRMount>();

#if UNITY_EDITOR
		[Header("Debug")]
		public string customMountName;
#endif
		public void OnEnable() {
			if (!VanillaVR.i) {
				Error("No VanillaVR instance found.");
				return;
			}

			switch (handedness) {
				case Handedness.Left:
					VanillaVR.i.leftTracker = this;

					inputID = 0;

					target = VanillaVR.i.rigScene.leftTrackerTarget;
					break;
				case Handedness.Middle:
					VanillaVR.i.hmdTracker = this;

					target = VanillaVR.i.rigScene.hmdTrackerTarget;

					//VanillaScreenFader.i.drawTransform = transform;
					break;
				case Handedness.Right:
					VanillaVR.i.rightTracker = this;

					inputID = 1;

					target = VanillaVR.i.rigScene.rightTrackerTarget;
					break;
			}

			ResetTrackerPositions();

			VanillaVR.i.onTranslocation.AddListener(ResetTrackerPositions);
		}

		public void ResetTrackerPositions() {
			physicsChase.target = target;
			physicsRotate.target = target;

			transform.position = target.position;
			transform.rotation = target.rotation;
		}

		public void OnDisable() {
			if (!VanillaVR.i) {
				if (!Vanilla.applicationQuitBegun) {
					Error("No VanillaVR instance found.");
				}

				return;
			}

			switch (handedness) {
				case Handedness.Left:
					VanillaVR.i.leftTracker = null;
					break;
				case Handedness.Middle:
					VanillaVR.i.hmdTracker = null;
					break;
				case Handedness.Right:
					VanillaVR.i.rightTracker = null;
					break;
			}

			VanillaVR.i.onTranslocation.RemoveListener(ResetTrackerPositions);
		}

		public VRMount CreateMount(VRMountConfig asset) {
			if (asset == null) {
				Error("The passed asset is null.");
				return null;
			}

			if (asset.mountPrefab == null) {
				Error("The requested VR Tracker Mount [{0}] has no assigned prefab and cannot be loaded as a result.", asset);
				return null;
			}

			if (!asset.IsCompatibleWithTracker(handedness)) {
				Warning("The requested VR Tracker Mount [{0}] is not compatible with this tracker [{1}].", asset, handedness);
				return null;
			}

			if (MountAlreadyCreated(asset)) {
				Warning("The requested mount object [{0}] has already been created for this VR tracker [{1}]", asset.mountPrefab.name, handedness);
				return mounts[asset];
			}

			VRMount newMount = Instantiate(asset.mountPrefab, transform.position, transform.rotation, transform).GetComponentInChildren<VRMount>();

			if (newMount == null) {
				Error("No VRMount script or inherited class is attached to the instantiated mount prefab [{0}]. Make sure your prefab has a VRMount or VRMount-inherited component somewhere in its hierarchy.", asset.mountPrefab);
				return null;
			}

			mounts.Add(asset, newMount);

			newMount.Initialize();

			return newMount;
		}

		public void ActivateMount(VRMountConfig asset) {
			if (!MountAlreadyCreated(asset)) {
				Warning("That mount [{0}] hasn't been created yet, so it can't be deactivated.", asset);
			}
		}

		public void DeactivateMount(string mountName) {

		}

		public void DestroyMount(VRMountConfig asset) {
			if (!MountAlreadyCreated(asset)) {
				Warning("The requested mount [{0}] can't be destroyed because it doesn't exist yet on the [{1}] tracker", asset, handedness);
				return;
			}

			mounts[asset].Remove();

			mounts.Remove(asset);
		}

		public VRMount GetMount(VRMountConfig asset) {
			if (MountAlreadyCreated(asset)) {
				return mounts[asset];
			}else {
				return null;
			}
		}

		public bool MountAlreadyCreated(VRMountConfig asset) {
			return mounts.ContainsKey(asset);
		} 
	}
}