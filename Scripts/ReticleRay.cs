﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Vanilla;
using Vanilla.VR;

public class ReticleRay : MonoBehaviour {


	GameObject origin;
	GameObject playerHead;
	private Vector3 originalScale;
	public GameObject reticle;
	public enum RayType { head, hand };
	public RayType rayType;
	LineRenderer line;
	float distance = 10f;

	public Material lineMaterial;

	bool displayLine;

	// Use this for initialization
	void Start() {
		SetupRay();
	}

	// Update is called once per frame
	void Update() {
		FireRay();
	}
	public void SetupRay() {
		origin = this.gameObject;

		if (playerHead == null)
			playerHead = VanillaVR.i.rigScene.hmdTrackerTarget.gameObject;

		originalScale = reticle.transform.localScale;

		if (rayType == RayType.hand) {
			displayLine = true;
			line = this.gameObject.AddComponent<LineRenderer>();
			line.positionCount = 2;
			line.startWidth = .005f;
			line.endWidth = .005f;
			line.material = lineMaterial;
		}


	}
	public void FireRay() {
		RaycastHit hit;
		bool hitOccured;

		if (Physics.Raycast(new Ray(origin.transform.position, origin.transform.rotation * Vector3.forward),
							 out hit)) {
			distance = hit.distance;
			hitOccured = true;
		} else {
			//if(rayType == RayType.head) distance = playerHead.farClipPlane * 0.1f;
			hitOccured = false;
			distance = 10;
		}

		reticle.transform.position = origin.transform.position + origin.transform.rotation * Vector3.forward * distance;
		reticle.transform.LookAt(playerHead.transform.position);

		if (hitOccured)
			distance = Vector3.Distance(reticle.transform.position, playerHead.transform.position);

		reticle.transform.localScale = originalScale * distance;

		if (displayLine) {
			line.SetPosition(0, reticle.transform.position);
			line.SetPosition(1, this.transform.position);

		}


	}


}
