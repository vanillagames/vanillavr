﻿using UnityEngine;

using System;

using Vanilla.ThreeDee;

namespace Vanilla.VR {
	public class Reticle : VanillaBehaviour {

		public ReticleRotationMode reticleRotationMode;

		public Vector3 originalScale;
		public Transform cameraTransform;

		public enum ReticleRotationMode {
			None,
			AlignToHit,
			LookAtMainCamera
		}

		void Awake() {
			originalScale = transform.localScale;
		}

		public void CacheCameraTransform(Transform t) {
			cameraTransform = t;
		}

		void UpdateRotation() {
			switch (reticleRotationMode) {
				case ReticleRotationMode.LookAtMainCamera:
					if (cameraTransform) {
						transform.LookAt(cameraTransform);
					}
					break;
			}
		}

		void UpdateScale() {
			transform.localScale = originalScale * Vector3.Distance(transform.position, cameraTransform.position);
		}
	}
}