﻿using UnityEngine;

using System;

using Vanilla.Inputs;

namespace Vanilla.VR {
	public class DebugRigController : VanillaBehaviour {

		public float xSensitivity;
		public float ySensitivity;

		public float smoothing;

		public Transform[] trackers;

		Vector3[] targetRotations = new Vector3[3];

		public Camera mainCamera;

		//void OnEnable() {
		//	if (!VanillaInput.i) {
		//		return;
		//	}

		// [ToDo] Put all mouse-related events in VanillaInput.
		//VanillaInput.i.SubscribeToAxisFloat("Mouse", 0, OnMouseXChanged);
		//VanillaInput.i.SubscribeToAxisFloat("Mouse", 1, OnMouseYChanged);
		//}

		//void OnDisable() {

		//}

		void Awake() {
			mainCamera = GetComponentInChildren<Camera>();
		}

		void Update() {
			for (int i = 0; i < 3; i++) {
				if (Input.GetMouseButton(i)) {
					RotateWithMouse(i);
				}

				UpdateRotation(i);
			}
		}

		void RotateWithMouse(int i) {
			targetRotations[i] += new Vector3(-Input.GetAxis("Mouse Y") * ySensitivity, Input.GetAxis("Mouse X") * xSensitivity, 0);

			targetRotations[i].z = 0;
		}

		void UpdateRotation(int i) {
			trackers[i].localRotation = Quaternion.Lerp(trackers[i].localRotation, Quaternion.Euler(targetRotations[i]), Time.deltaTime * smoothing);
		}

		//public void OnMouseXChanged(float xMovement) {
		// [ToDo] Put all mouse-related events in VanillaInput.
		//}

		//public void OnMouseYChanged(float yMovement) {
		// [ToDo] Put all mouse-related events in VanillaInput.
		//}
	}
}