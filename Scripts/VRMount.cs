﻿using UnityEngine;
using System.Collections;

using Vanilla.ThreeDee;

namespace Vanilla.VR {
	public class VRMount : VanillaBehaviour {

		[Header("[ VR Mount ]")]
		[ReadOnly]
		private VRTracker _tracker;
		public VRTracker tracker {
			get {
				if (!_tracker) {
					_tracker = GetComponentInParent<VRTracker>();
				}

				return _tracker;
			}
			set {
				_tracker = value;
			}
		}

		public VRMountConfig config;

		//private PhysicsAttachToTarget _attachment;
		//public PhysicsAttachToTarget attachment {
		//	get {
		//		if (!_attachment) {
		//			_attachment = GetComponent<PhysicsAttachToTarget>();
		//		}

		//		return _attachment;
		//	}
		//	set {
		//		_attachment = value;
		//	}
		//}

		// The current problem is that, no matter what you try, you can't get the TrackerMounts to move to their respective Trackers
		// on creation. It's really annoying because otherwise they spawn at 0,0,0 and try to move over to their targets.
		// You could try instantiating them with the location of the trackers as parameters.

		//public void OnEnable() {
		//VanillaSceneLoader.i.onSceneLoadSync.AddListener(ResetTrackerMountPosition);
		//VanillaVR.i.onRigTransl.AddListener(ResetTrackerMountPosition);
		//}

		public void ResetTrackerMountPosition() {
			transform.position = tracker.transform.position;
			transform.rotation = tracker.transform.rotation;


			//switch (tracker.handedness) {
			//	case Handedness.Left:
			//		transform.position = VanillaVR.i.rigScene.leftTrackerTarget.position;
			//		transform.rotation = VanillaVR.i.rigScene.leftTrackerTarget.rotation;
			//		break;
			//	case Handedness.Right:
			//		transform.position = tracker.transform.position;
			//		transform.rotation = tracker.transform.rotation;
			//		break;
			//	case Handedness.None:
			//		transform.position = tracker.transform.position;
			//		transform.rotation = tracker.transform.rotation;
			//		break;
			//}
		}

		//public void OnDisable() {
		//VanillaSceneLoader.i.onSceneLoadSync.AddListener(ResetTrackerMountPosition);
		//VanillaVR.i.onRigTransl.RemoveListener(ResetTrackerMountPosition);
		//VanillaAnchors.dictionary["VR Rig"].;
		//}

		public void Initialize() {
			gameObject.name = string.Format("[{0}] [{1}]", config.mountPrefab.name, tracker.handedness);

			Log("A new mount was instantiated [{0}]", gameObject);

			MountSpecificSetup();
		}

		public virtual void MountSpecificSetup() {}

		public virtual void MountSpecificRemoval() {}

		public void Remove() {
			MountSpecificRemoval();

			Destroy(gameObject);
		}
	}
}