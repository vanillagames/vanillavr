﻿using UnityEngine;
using UnityEngine.UI;

using System.Collections;

using Vanilla.Math;
using Vanilla.Transitions;

namespace Vanilla.VR {
	public class VanillaVRLoadingScreen : LoadingScreen {

		[Header("[ Vanilla VR Loading Screen ]")]

		public VanillaSceneAsset loadingScreenScene;

		public TransitionConfiguration fadeToClear;
		
		public TransitionConfiguration fadeToColour;

		public bool proceed;

		int defaultMask;

		public override IEnumerator Begin() {
			Log("Running 'begin' coroutine.");

			// Get the rig camera
			//Camera hmdCam = VanillaVR.i.rigScene.rigCamera.GetComponent<Camera>();

			//VanillaCameras.i.Getc

			//VanillaCameras.i.GetCamera("HMD").enabled = false;

			VanillaCameras.i.GetCamera("HMD").cullingMask = LayerMask.GetMask();



			// Get the original layer mask setup
			//defaultMask = VanillaCameras.i.GetCamera("HMD").cullingMask;

			yield return VanillaTransitions.i.Animate(fadeToColour);

			if (loadingScreenScene) {
				yield return VanillaSceneLoader.i.LoadScene(loadingScreenScene, UnityEngine.SceneManagement.LoadSceneMode.Additive);
			}

			//if (loadingScreenScene) {
			//	VanillaSceneLoader.i.ActivateUnityScene(loadingScreenScene.name);
			//}

			// Now that the 'lights are down', lets make the rig camera only able to see System objects
			//hmdCam.cullingMask = ayerMask.GetMask("System");
			//hmdCam.cullingMask = 1 >> LayerMask.GetMask("System");

			yield return VanillaTransitions.i.Animate(fadeToClear);

			//yield return VanillaTime.i.GetUnscaledWait(1);

			//yield return new WaitForSeconds(5.0f);

			Log("Finishing 'begin' coroutine.");

			yield break;
		}

		public override IEnumerator End() {
			Log("Running 'end' coroutine.");



			//Camera hmdCam = VanillaVR.i.rigScene.rigCamera.GetComponent<Camera>();

			//yield return Vanilla.waitForSeconds[9];
			//yield return new WaitForSeconds(120);

			//yield return new WaitForSeconds(30.0f);

			yield return VanillaTransitions.i.Animate(fadeToColour);

			VanillaCameras.i.GetCamera("HMD").cullingMask = LayerMask.GetMask("Default");

			if (loadingScreenScene) {
				VanillaSceneLoader.i.LazyUnload(loadingScreenScene);
			}

			//yield return VanillaSceneLoader.i.RunVanillaSceneActivation(VanillaSceneLoader.i.currentSetup.sceneAssets[VanillaSceneLoader.i.currentSetup.sceneToActivateOnLoadCompletion].name);



			// Now that the lights are back down, lets return the rig camera to its original culling mask
			//hmdCam.cullingMask = defaultMask;

			//hmdCam.enabled = true;
			//VanillaCameras.i.GetCamera("HMD").enabled = true;

			//yield return VanillaTransitions.i.Animate(fadeToClear);

			//yield return VanillaSceneLoader.i.RunVanillaSceneActivation(VanillaSceneLoader.i.currentSetup.sceneAssets[VanillaSceneLoader.i.currentSetup.sceneToActivateOnLoadCompletion].name);

			Log("Finishing 'end' coroutine.");

			yield break;
		}
	}
}