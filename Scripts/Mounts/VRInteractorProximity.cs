﻿//using UnityEngine;

//using System;

//using System.Collections;
//using System.Collections.Generic;

//using Vanilla.Inputs;

//namespace Vanilla.VR {
//	public class VRInteractorProximity : VRInteractor {

//		//[Header("[ VR Interactor With Proximity ]")]

//		//[ReadOnly]
//		//[Tooltip("Is more than one interactable in range? Distance checking is only run if so.")]
//		//public bool multipleInteractablesInRange;

//		//[Tooltip("This is the current nearest interactable.")]
//		//[ReadOnly]
//		//public VRInteractable currentNearest;

//		//public float distanceCheckRateInSeconds = 0.5f;

//		//public void OnEnable() {
//		//	InvokeRepeating("CheckForInteractableDistanceChanges", 0, distanceCheckRateInSeconds);
//		//}

//		//public void OnDisable() {
//		//	CancelInvoke("CheckForInteractableDistanceChanges");
//		//}

//		//public void CheckForInteractableDistanceChanges() {
//		//	if (multipleInteractablesInRange) {
//		//		CompareInteractableDistances();
//		//	}
//		//}

//		void CompareInteractableDistances() {
//			float shortestSqrMag = float.PositiveInfinity;
//			int shortestIndex = 0;

//			for (int i = 0; i < inRangeInteractables.Count; i++) {
//				float sqrMag = (inRangeInteractables[i].transform.position - rangeTrigger.transform.position).sqrMagnitude;

//				if (sqrMag < shortestSqrMag) {
//					shortestSqrMag = sqrMag;
//					shortestIndex = i;
//				}
//			}

//			if (currentNearest != inRangeInteractables[shortestIndex]) {
//				HandleNewNearest(inRangeInteractables[shortestIndex]);
//			}
//		}

//		void AssessRangeCount() {
//			switch (inRangeInteractables.Count) {
//				case 0:
//					multipleInteractablesInRange = false;

//					HandleNewNearest(null);
//					break;
//				case 1:
//					multipleInteractablesInRange = false;

//					HandleNewNearest(inRangeInteractables[0]);
//					break;
//				default:
//					multipleInteractablesInRange = true;
//					break;
//			}
//		}

//		public void HandleNewNearest(VRInteractable newNearest) {
//			if (currentNearest && currentNearest.canInteract) {
//				currentNearest.OnNoLongerNearest(this);
//			}

//			currentNearest = newNearest;

//			if (currentNearest && currentNearest.canInteract) {
//				currentNearest.OnBecameNearest(this);
//			}

//			HandleNewNearestExt();
//		}

//		public override void OnTriggerEnterExt(VRInteractable interactable) {
//			AssessRangeCount();
//		}

//		public override void OnTriggerExitExt(VRInteractable interactable) {
//			AssessRangeCount();

//			if (interactable == currentNearest) {
//				currentNearest.OnNoLongerNearest(this);
//			}
//		}
		
//		/// <summary>
//		/// This is an empty extendable function that runs once the new nearest Interactable has been decided.
//		/// </summary>
//		public virtual void HandleNewNearestExt() {

//		}
//	}
//}