﻿using UnityEngine;

using System;

using System.Collections;
using System.Collections.Generic;

using Vanilla.Inputs;

namespace Vanilla.VR {
#if UNITY_EDITOR
	using UnityEditor;


	[CustomEditor(typeof(VRInteractor), true)]
	public class VRInteractorEditor : VanillaEditor {

		VRInteractor interactor;

		void OnEnable() {
			interactor = (VRInteractor)behaviour;
		}

		public override void OnInspectorGUI() {
			base.OnInspectorGUI();

			if (Application.isPlaying) {
				if (GUILayout.Button("Force Trigger Click")) {
					interactor.TriggerClick();
				}

				if (GUILayout.Button("Force Trigger Release")) {
					interactor.TriggerRelease();
				}

				if (GUILayout.Button("Force Grip Click")) {
					interactor.GripClick();
				}

				if (GUILayout.Button("Force Grip Release")) {
					interactor.GripRelease();
				}
			}

			 
		}
	}
#endif

	public class VRInteractor : VRMount {

		[Header("[ VR Interactor ]")]

		private Rigidbody _rigidbody;
		public new Rigidbody rigidbody {
			get {
				if (!_rigidbody) {
					_rigidbody = GetComponent<Rigidbody>(); // It has to be attached to receive OnTriggerEnter/Exits

					if (!_rigidbody) {
						Log("No rigidbody is attached!");
					}
				}

				return _rigidbody;
			}
			set {
				_rigidbody = value;
			}
		}

		private Collider _rangeTrigger;
		public Collider rangeTrigger {
			get {
				if (!_rangeTrigger) {
					_rangeTrigger = GetComponent<Collider>(); // It has to be attached to receive OnTriggerEnter/Exits

					if (_rangeTrigger) {
						Log("No range trigger was pre-assigned, so this interactor mount will use the attached trigger collider.");
					} else {
						Log("No range trigger was pre-assigned and no viable trigger collider is attached. As a result, interaction won't work for this component.");
					}
				}

				return _rangeTrigger;
			}
			set {
				_rangeTrigger = value;
			}
		}

		[EnumFlags(1)]
		public VRInteractionFlags interactionFlags;

		[ReadOnly]
		public List<VRInteractable> inRangeInteractables = new List<VRInteractable>();

		//[ReadOnly]
		//public List<VRInteractable> clickedInteractables = new List<VRInteractable>();

		[ReadOnly]
		public VRInteractable[] triggerInteractables = new VRInteractable[10];

		[ReadOnly]
		public VRInteractable[] gripInteractables = new VRInteractable[10];
		//public List<VRInteractable> gripInteractables = new List<VRInteractable>();

		[Tooltip("This is the current nearest interactable.")]
		[ReadOnly]
		public VRInteractable nearestInteractable;

		[ReadOnly]
		public static VRInteractor leftInteractor, rightInteractor, headInteractor;

		VRInteractable iTemp;

		[ReadOnly]
		public bool triggerDown;

		[ReadOnly]
		public bool gripDown;

		public float distanceCheckRateInSeconds = 0.5f;

		[Header("[ Pick-up Settings ]")]

		[ReadOnly]
		public VRInteractor otherHand;

		///--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
		/// Proximity Checks
		///--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

		public void Awake() {
			// If we're using VanillaVRs custom interaction layer (a good idea for optimization!)
			if (VanillaVR.i && VanillaVR.i.useCustomInteractionLayer) {
				// Change the layer for whichever object has the rangeTrigger to the custom interaction layer.
				rangeTrigger.gameObject.layer = VanillaVR.i.interactionLayer;
			}
		}

		public override void MountSpecificSetup() {
			switch (tracker.handedness) {
				case Handedness.Left:
					leftInteractor = this;

					if (rightInteractor != null) {
						rightInteractor.otherHand = this;
					}

					if (VanillaInputs.i) {
						VanillaInputs.i.Subscribe(ProjectAction.LeftInteractorClick, TriggerClick);
						VanillaInputs.i.Subscribe(ProjectAction.LeftInteractorRelease, TriggerRelease);

						VanillaInputs.i.Subscribe(ProjectAction.LeftInteractorGripClick, GripClick);
						VanillaInputs.i.Subscribe(ProjectAction.LeftInteractorGripRelease, GripRelease);
					} else {
						Warning("Can't subscribe to inputs events; Vanilla Inputs is not currently available");
					}
					break;
				case Handedness.Right:
					rightInteractor = this;

					if (leftInteractor != null) {
						leftInteractor.otherHand = this;
					}

					if (VanillaInputs.i) {
						VanillaInputs.i.Subscribe(ProjectAction.RightInteractorClick, TriggerClick);
						VanillaInputs.i.Subscribe(ProjectAction.RightInteractorRelease, TriggerRelease);

						VanillaInputs.i.Subscribe(ProjectAction.RightInteractorGripClick, GripClick);
						VanillaInputs.i.Subscribe(ProjectAction.RightInteractorGripRelease, GripRelease);
					} else {
						Warning("Can't subscribe to inputs events; Vanilla Inputs is not currently available");
					}
					break;
				case Handedness.Middle:
					headInteractor = this;
					break;
			}
		}

		public override void MountSpecificRemoval() {
			switch (tracker.handedness) {
				case Handedness.Left:
					leftInteractor = null;

					if (VanillaInputs.i) {
						VanillaInputs.i.Unsubscribe(ProjectAction.LeftInteractorClick, TriggerClick);
						VanillaInputs.i.Unsubscribe(ProjectAction.LeftInteractorRelease, TriggerRelease);

						VanillaInputs.i.Unsubscribe(ProjectAction.LeftInteractorGripClick, GripClick);
						VanillaInputs.i.Unsubscribe(ProjectAction.LeftInteractorGripRelease, GripRelease);
					} else {
						Warning("Can't unsubscribe from inputs events; Vanilla Inputs is not currently available.");
					}
					break;
				case Handedness.Right:
					rightInteractor = null;

					if (VanillaInputs.i) {
						VanillaInputs.i.Unsubscribe(ProjectAction.RightInteractorClick, TriggerClick);
						VanillaInputs.i.Unsubscribe(ProjectAction.RightInteractorRelease, TriggerRelease);

						VanillaInputs.i.Unsubscribe(ProjectAction.RightInteractorGripClick, GripClick);
						VanillaInputs.i.Unsubscribe(ProjectAction.RightInteractorGripRelease, GripRelease);
					} else {
						Warning("Can't unsubscribe from inputs events; Vanilla Inputs is not currently available.");
					}
					break;
				case Handedness.Middle:
					headInteractor = null;
					break;
			}
		}

		public virtual void OnTriggerEnter(Collider other) {
			// If this interactor can't interact, bail out early.
			if (!interactionFlags.HasFlag(VRInteractionFlags.allowInteraction)) {
				return;
			}

			// If the collider we entered is one of the other interactors, bail out early.
			if (ColliderIsAnInteractor(other)) {
				return;
			}

			//Log("My range trigger entered something interactive! [{0}]", other.gameObject.name);

			iTemp = other.GetComponentInParent<VRInteractable>();

			if (iTemp == null) {
				Error("It seems that a trigger was entered [{0}] that has no VRInteractable component on itself or any parent.", other.gameObject);
				return;
			}

			// If this interactable is already added, bail out early..
			if (inRangeInteractables.Contains(iTemp)) {
				return;
			}

			// Either way, track that its in range...
			inRangeInteractables.Add(iTemp);	

			// ...and if this interactor can send proximity calls...
			if (interactionFlags.HasFlag(VRInteractionFlags.allowProximityTracking)) {
				// ...do that.
				iTemp.OnInteractorEnter(this);

				// ...then do different stuff depending on how many interactables are now in range.
				AssessRangeCount();
			}

			TriggerEntered(other);
		}

		public virtual void OnTriggerExit(Collider other) {
			//Log("My range trigger exited something interactive! [{0}]", other.gameObject.name);

			if (!interactionFlags.HasFlag(VRInteractionFlags.allowInteraction)) {
				return;
			}

			if (ColliderIsAnInteractor(other)) {
				return;
			}

			iTemp = other.GetComponentInParent<VRInteractable>();

			if (iTemp == null) {
				Error("It seems that a trigger was exited [{0}] that isn't a VRInteractable.", other.gameObject);
				return;
			}

			// If our list of nearby interactables doesn't contain this interactable, bail out early... but how was it not added in the first place?
			if (!inRangeInteractables.Contains(iTemp)) {
				return;
			}

			// Either way, track that its gone...
			inRangeInteractables.Remove(iTemp);

			// Finally, if this interactor can send proximity calls...
			if (interactionFlags.HasFlag(VRInteractionFlags.allowProximityTracking)) {
				// ...do that.
				iTemp.OnInteractorExit(this);

				// ...then do different stuff depending on how many interactables are still in range.
				AssessRangeCount();
			}

			TriggerExited(other);
		}

		public virtual void TriggerClick() {
			if (!interactionFlags.HasFlag(VRInteractionFlags.allowTrigger) || triggerDown) {
				return;
			}

			triggerDown = true;

			//clickedInteractables = inRangeInteractables.ToList();

			inRangeInteractables.CopyTo(triggerInteractables);

			for (int i = 0; i < triggerInteractables.Length; i++) {
				if (triggerInteractables[i] != null) {
					triggerInteractables[i].OnTriggerClick(this);
				}
			}

			TriggerClickDown();
		}

		public virtual void TriggerRelease() {
			if (!interactionFlags.HasFlag(VRInteractionFlags.allowTrigger) || !triggerDown) {
				return;
			}

			triggerDown = false;

			for (int i = 0; i < triggerInteractables.Length; i++) {
				if (triggerInteractables[i] != null) {
					triggerInteractables[i].OnTriggerRelease(this);
					triggerInteractables[i] = null;
				}
			}

			//clickedInteractables.Clear();

			TriggerClickUp();
		}

		public virtual void GripClick() {
			if (!interactionFlags.HasFlag(VRInteractionFlags.allowGrip) || gripDown) {
				return;
			}

			gripDown = true;

			inRangeInteractables.CopyTo(gripInteractables);
			//gripInteractables = inRangeInteractables;

			for (int i = 0; i < gripInteractables.Length; i++) {
				if (gripInteractables[i] != null) {
					gripInteractables[i].OnGripClick(this);
				}
			}
		}

		public virtual void GripRelease() {
			if (!interactionFlags.HasFlag(VRInteractionFlags.allowGrip) || !gripDown) {
				return;
			}

			gripDown = false;

			for (int i = 0; i < gripInteractables.Length; i++) {
				if (gripInteractables[i] != null) {
					gripInteractables[i].OnGripRelease(this);
					gripInteractables[i] = null;
				}
			}

			//gripInteractables.Clear();
		}

		///--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
		/// Interaction Extensions
		///--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

		public virtual void TriggerEntered(Collider other) {

		}

		public virtual void TriggerExited(Collider other) {

		}

		public virtual void TriggerClickDown() {

		}

		public virtual void TriggerClickUp() {

		}

		///--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
		/// Proximity Checking
		///--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

		void AssessRangeCount() {
			switch (inRangeInteractables.Count) {
				case 0:
					HandleNewNearest(null);
					break;
				case 1:
					// If only one interactable is in range, cease any distance checking that may have been happening.
					CancelInvoke("CompareInteractableDistances");

					// And consider the solo interactable the new nearest.
					HandleNewNearest(inRangeInteractables[0]);
					break;
				default:
					InvokeRepeating("CompareInteractableDistances", 0, distanceCheckRateInSeconds);
					break;
			}
		}

		void CompareInteractableDistances() {
			float shortestSqrMag = float.PositiveInfinity;
			int shortestIndex = 0;

			for (int i = 0; i < inRangeInteractables.Count; i++) {
				float sqrMag = (inRangeInteractables[i].transform.position - rangeTrigger.transform.position).sqrMagnitude;

				if (sqrMag < shortestSqrMag) {
					shortestSqrMag = sqrMag;
					shortestIndex = i;
				}
			}

			if (nearestInteractable != inRangeInteractables[shortestIndex]) {
				HandleNewNearest(inRangeInteractables[shortestIndex]);
			}
		}

		public void HandleNewNearest(VRInteractable newNearest) {
			if (nearestInteractable) {
				nearestInteractable.OnNoLongerNearest(this);
			}

			nearestInteractable = newNearest;

			if (nearestInteractable) {
				nearestInteractable.OnBecameNearest(this);
			}
		}

		///--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
		/// Helpers
		///--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

		public bool ColliderIsAnInteractor(Collider other) {
			return (leftInteractor && other == leftInteractor.rangeTrigger) || (rightInteractor && other == rightInteractor.rangeTrigger) || (headInteractor && other == headInteractor.rangeTrigger);
		}
	}
}