﻿using UnityEngine;

using System;

namespace Vanilla.VR {
	public class PhysicsTestMount : VRMount {
		[Header("Physics Test Mount")]
		public Color leftColor = Color.blue;
		public Color rightColor = Color.red;

		public override void MountSpecificSetup() {
			MeshRenderer renderer = GetComponent<MeshRenderer>();
			renderer.material = new Material(renderer.material);

			switch (tracker.handedness) {
				case Handedness.Left:
					renderer.material.color = leftColor;
					break;
				case Handedness.Right:
					renderer.material.color = rightColor;
					break;
			}
		}
	}
}