﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

using System;

namespace Vanilla.VR {
	public class VRPointerEvents : VanillaBehaviour, IPointerDownHandler, IPointerUpHandler, IPointerEnterHandler, IPointerExitHandler {

		public UnityEvent LeftOnPointerEnter;
		public UnityEvent LeftOnPointerExit;
		public UnityEvent LeftOnPointerDown;
		public UnityEvent LeftOnPointerUp;

		public UnityEvent MiddleOnPointerEnter;
		public UnityEvent MiddleOnPointerExit;
		public UnityEvent MiddleOnPointerDown;
		public UnityEvent MiddleOnPointerUp;

		public UnityEvent RightOnPointerEnter;
		public UnityEvent RightOnPointerExit;
		public UnityEvent RightOnPointerDown;
		public UnityEvent RightOnPointerUp;


		void Awake() {
			if (LeftOnPointerEnter == null) {
				LeftOnPointerEnter = new UnityEvent();
			}

			if (LeftOnPointerExit == null) {
				LeftOnPointerExit = new UnityEvent();
			}

			if (LeftOnPointerDown == null) {
				LeftOnPointerDown = new UnityEvent();
			}

			if (LeftOnPointerUp == null) {
				LeftOnPointerUp = new UnityEvent();
			}

			if (MiddleOnPointerEnter == null) {
				MiddleOnPointerEnter = new UnityEvent();
			}

			if (MiddleOnPointerExit == null) {
				MiddleOnPointerExit = new UnityEvent();
			}

			if (MiddleOnPointerDown == null) {
				MiddleOnPointerDown = new UnityEvent();
			}

			if (MiddleOnPointerUp == null) {
				MiddleOnPointerUp = new UnityEvent();
			}

			if (RightOnPointerEnter == null) {
				RightOnPointerEnter = new UnityEvent();
			}

			if (RightOnPointerExit == null) {
				RightOnPointerExit = new UnityEvent();
			}

			if (RightOnPointerDown == null) {
				RightOnPointerDown = new UnityEvent();
			}

			if (RightOnPointerUp == null) {
				RightOnPointerUp = new UnityEvent();
			}
		}

		public void OnPointerEnter(PointerEventData eventData) {
			switch (eventData.button) {
				case PointerEventData.InputButton.Left:
					LeftOnPointerEnter.Invoke();
					break;
				case PointerEventData.InputButton.Middle:
					MiddleOnPointerEnter.Invoke();
					break;
				case PointerEventData.InputButton.Right:
					RightOnPointerEnter.Invoke();
					break;
			}
		}

		public void OnPointerExit(PointerEventData eventData) {
			switch (eventData.button) {
				case PointerEventData.InputButton.Left:
					LeftOnPointerExit.Invoke();
					break;
				case PointerEventData.InputButton.Middle:
					MiddleOnPointerExit.Invoke();
					break;
				case PointerEventData.InputButton.Right:
					RightOnPointerExit.Invoke();
					break;
			}
		}

		public void OnPointerDown(PointerEventData eventData) {
			switch (eventData.button) {
				case PointerEventData.InputButton.Left:
					LeftOnPointerDown.Invoke();
					break;
				case PointerEventData.InputButton.Middle:
					MiddleOnPointerDown.Invoke();
					break;
				case PointerEventData.InputButton.Right:
					RightOnPointerDown.Invoke();
					break;
			}
		}

		public void OnPointerUp(PointerEventData eventData) {
			switch (eventData.button) {
				case PointerEventData.InputButton.Left:
					LeftOnPointerUp.Invoke();
					break;
				case PointerEventData.InputButton.Middle:
					MiddleOnPointerUp.Invoke();
					break;
				case PointerEventData.InputButton.Right:
					RightOnPointerUp.Invoke();
					break;
			}
		}
	}
}